import blessed from 'blessed';

const screen = blessed.screen({autoPadding: true});

const list = blessed.list({
  parent: screen,
  height: '70%',
  border: 'line',
  label: 'Packages',
  keys: true,
  scrollable: true,
  style: {
    selected: {
      bg: 'blue',
    },
  },
});

list.focus();

const logger = blessed.log({
  parent: screen,
  top: '70%',
  height: '30%',
  border: 'line',
  label: 'Logger',
});

blessed.box({
  parent: screen,
  bottom: 0,
  height: 1,
  padding: {left: 2, right: 2},
  content: [
    '{bold}Up{/}/{bold}Down{/} - Select',
    '{bold}r{/} - Reload',
    '{bold}enter{/} - Query',
    '{bold}d{/} - Disable',
    '{bold}e{/} - Enable',
    '{bold}q{/} - Exit',
  ].join('  |  '),
  tags: true,
});

screen.key('q', () => {
  eventHandler('exit');
});
screen.key('r', () => {
  eventHandler('reload');
});
screen.key('d', () => {
  eventHandler('disable', list.getItem(list.selected).content);
});
screen.key('e', () => {
  eventHandler('enable', list.getItem(list.selected).content);
});
screen.key('enter', () =>
  eventHandler('query', list.getItem(list.selected).content),
);
screen.render();

export function setListItems(items) {
  list.setItems(items);
  screen.render();
}

export function log(text) {
  if (text) {
    logger.add(
      text
        .split('\n')
        .filter((s) => s)
        .join('\n'),
    );
  }
}

let eventHandler = null;
export function setEventHandler(cb) {
  eventHandler = cb;
}
