import {listPackages, disablePackage, enablePackage} from './adb.js';
import {setListItems, setEventHandler, log} from './layout.js';
import open from 'open';

const handlers = {
  exit() {
    process.exit(0);
  },
  query(item) {
    open(`https://www.google.com/search?q=${item}`);
  },
  disable(item) {
    disablePackage(item, (info) => log(info));
  },
  enable(item) {
    enablePackage(item, (info) => log(info));
  },
  reload() {
    listPackages(
      (items) => {
        setListItems(items);
        log(`RELOAD ${items.length} packages listed.`);
      },
      (err) => {
        log(err);
      },
    );
  },
};

setEventHandler((ev, data) => {
  const h = handlers[ev];
  if (h) h(data);
});

handlers['reload']();
